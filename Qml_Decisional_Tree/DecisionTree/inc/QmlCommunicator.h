#ifndef QMLCOMMUNICATOR_H
#define QMLCOMMUNICATOR_H

#include <QObject>
#include <QVariant>
class QQmlComponent;

class QMLCommunicator : public QObject
{
    Q_OBJECT
    //Q_PROPERTY(Qstring GetId READ GetId)
    Q_PROPERTY(int GetWidth READ GetWidth NOTIFY WidthChanged)
    Q_PROPERTY(int GetHeight READ GetHeight NOTIFY HeightChanged)
    Q_PROPERTY(QString GetText READ GetText WRITE ItWasCreated NOTIFY ObjectCompletedSgn)
    Q_PROPERTY(QString Color READ GetColor WRITE SetColor NOTIFY ColorChanged)
    Q_PROPERTY(QString Pressed READ GetMouseStatus WRITE ItWasPressed NOTIFY PressedChanged)

    Q_PROPERTY(QVariant IdValue READ GetId WRITE SetId NOTIFY IdChanged)
public:
    QMLCommunicator();
    QMLCommunicator(QQmlComponent* component);
    void SetComponent(QQmlComponent* component);

    void LastCreatedObject(QObject* obj);

    Q_INVOKABLE QString GetColor();
    Q_INVOKABLE void SetColor(QString color);
    Q_INVOKABLE const QString GetText() const;
    Q_INVOKABLE const int GetWidth() const;
    Q_INVOKABLE const int GetHeight() const;
    Q_INVOKABLE QString GetMouseStatus();

    Q_INVOKABLE void ItWasCreated(QString id);
    Q_INVOKABLE void ItWasPressed(QString id);

    Q_INVOKABLE QVariant GetId();
    Q_INVOKABLE void SetId(QVariant id);

signals:
    void WidthChanged();
    void HeightChanged();
    void ObjectCompletedSgn();
    void ColorChanged();
    void PressedChanged();
    void IdChanged();
private:
    int Id;
    QQmlComponent* component;
    QObject* _lastOne;

};

#endif // QMLCOMMUNICATOR_H
