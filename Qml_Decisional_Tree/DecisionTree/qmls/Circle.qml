import QtQuick 2.0

Rectangle {
    id: circle
    width: 50
    height: 50
    radius: 50*0.5
    z: 0
    color: "white"
    x: 0
    y: 0
    property point beginDrag
    property bool caught: false

    signal clicked(string buttonId);
    border { width:2; color: "black" }

    Text {
        anchors.centerIn: parent
        text: "E"
        color: "black"
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:parent.clicked(parent.buttonId)
        drag.target: parent
        onPressed: {
            circle.beginDrag = Qt.point(circle.x, circle.y);

        }
        //onReleased:
    }
}
