import QtQuick 2.0

Rectangle {
    objectName: communicator.IdValue
    //id: communicator_GetId
    width: communicator.GetWidth
    height: communicator.GetHeight
    color: communicator.Color //"white"

    Text {
        anchors.centerIn: parent
        text: communicator.GetText
        color: "black"
    }

    Component.onCompleted:
        communicator.GetText = objectName

    MouseArea{
        anchors.fill: parent
        onPressed:
            communicator.Pressed = objectName
        onClicked:
            communicator.Pressed = "it was clicked " + objectName
    }

    function invokeMeOneMoreTime(message){
        console.log("Got message: ", message)
        return "some value"
    }

}


