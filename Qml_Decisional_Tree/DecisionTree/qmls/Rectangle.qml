//import QtQuick 2.0

//Item {
////    id: container
////    width: 500; height: 200;
////    property string test:"import QtQuick 2.0;Rectangle{id:abc;color:\;red\;;width:40;height:20;x:100;y:50}"
////    Rectangle {id:second; width:50;height:50;x:70;color:'green';
////    Component.onCompleted:{
////    var dynamicObject = Qt.createQmlObject(test,container,'firstObject');
////    }
////    } //end second

//id: container
//width: 800; height: 600;
//Rectangle {id:second; width:50;height:50;x:70;color:'green';
//Component.onCompleted:{var dynamicObject = Qt.createQmlObject(
//'import QtQuick 2.0;Rectangle{id:sample;width:40; height:40;color:"blue";}',
//container,'firstObject')
//}
//} //end second
//MouseArea {id:mouse1
//anchors.fill: parent;
//drag.target: second
//onPressed: {
//    //second.beginDrag = Qt.point(second.x, second.y);
//    var anotherObject =Qt.createQmlObject(
//                'import QtQuick 2.0;Rectangle{id:example;width:50; height:50;x:70;color:"red";}',container,'secondObject')
//}
//onReleased:{
//var anotherObject =Qt.createQmlObject(
//'import QtQuick 2.0;Rectangle{id:example;width:40; height:40;x:80;y:40;color:"pink";}',
//container,'secondObject')
//}//end onClicked
//}//end mouse1
//}


import QtQuick 2.0

Rectangle {
    id: rect
    width: 50
    height: 50
    z: 0
    color: "white"
    x: 0
    y: 0
    property point beginDrag
    property bool caught: false

    signal clicked(string buttonId);
    border { width:2; color: "black" }

    Text {
        anchors.centerIn: parent
        text: "D"
        color: "black"
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:parent.clicked(parent.buttonId)
        drag.target: parent
        onPressed: {
            rect.beginDrag = Qt.point(rect.x, rect.y);
        }
        //onReleased:
    }
}
