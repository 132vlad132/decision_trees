import QtQuick 2.4
import QtQuick.Window 2.2

Window {
    id: window
    title: "Decisional tree"
    visible: true
   // visibility: "Maximized"

    function invokeMe(message){
        console.log("Got MAIN message: ", message)
        //DynamicObject.invokeMeOneMoreTime(message)
        return "some value"
    }

    function createNewItem()
    {
        //Function creates 4 buttons
        var component = Qt.createComponent("DynamicObject.qml");

        var newComponent = component.createObject(window);



        //Connect the clicked signal of the newly created button
        //to the event handler buttonClicked.
        //newComponent.clicked.connect(buttonClicked)


    }

    Component.onCompleted: {
        createNewItem();
    }
}
//    Rectangle{
//        id:root

//        function buttonClicked(buttonId)
//        {
//            console.debug(buttonId);
//        }

//        function buttonPressed(buttonId)
//        {
//            console.debug("PRESSED: " + buttonId)
//        }

//        function createRectangle()
//        {
//            //Function creates 4 buttons
//            var component = Qt.createComponent("Rectangle.qml");

//            var buttonY = 0; //Button height : 50 + 5 unit margin
//            var button = component.createObject(root,{"x":0,"y":buttonY,"buttonId":1});



//            //Connect the clicked signal of the newly created button
//            //to the event handler buttonClicked.
//            button.clicked.connect(buttonClicked)

//        }

//        function createCircle()
//        {
//            var component2 = Qt.createComponent("Circle.qml");
//            var buttonY = 60; //Button height : 50 + 5 unit margin
//            var button2 = component2.createObject(root,{"x":0,"y":buttonY,"buttonId":2});
//            button2.clicked.connect(buttonClicked)
//        }

//        Component.onCompleted: {
//            createNewItem();
//            createRectangle();
//            createCircle();
//        }


//    }
//}
