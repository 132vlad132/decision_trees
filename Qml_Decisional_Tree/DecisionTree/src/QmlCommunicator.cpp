#include "QmlCommunicator.h"
#include <QDebug>
#include <QQmlComponent>


QMLCommunicator::QMLCommunicator():
    QObject(),
    Id(0)
{

}

QMLCommunicator::QMLCommunicator(QQmlComponent *component):
    QObject(),
    Id(0),
    component(component)
{

}

void QMLCommunicator::SetComponent(QQmlComponent *component)
{
    this->component = component;
    _lastOne = this->component->create();
}

void QMLCommunicator::LastCreatedObject(QObject *obj)
{
    _lastOne = &(*obj);
}

//const QString QMLCommunicator::GetId()
//{
//    ++Id;

//    return QString::number(Id);
//}

const QString QMLCommunicator::GetText() const
{
    QString value = QString(QString("\"Text to be displayed in the component ") + QString::number(Id) + QString("\""));
    return value;
}

const int QMLCommunicator::GetWidth() const
{
    return 50;
}

const int QMLCommunicator::GetHeight() const
{
    return 60;
}

QString QMLCommunicator::GetMouseStatus()
{
    component->create();
    return QString("I returned a value");
}

void QMLCommunicator::ItWasCreated(QString id)
{
    qDebug() << "QML object completed " + id;
}

void QMLCommunicator::ItWasPressed(QString id)
{
    QVariant returnedValue;
    QVariant message = "HALLO FROM C++";

    QMetaObject::invokeMethod(_lastOne, "invokeMe",
                              Q_RETURN_ARG(QVariant, returnedValue),
                              Q_ARG(QVariant, message));

    qDebug() << "Received from QML: " << returnedValue;

    qDebug() << "Button with id " << id << " was pressed";
}

QVariant QMLCommunicator::GetId()
{
    QVariant returnValue = Id;
    Id++;
    return returnValue;
}

void QMLCommunicator::SetId(QVariant id)
{
    qDebug() << "setID: " << id;
}

QString QMLCommunicator::GetColor()
{
    return QStringLiteral("white");
}

void QMLCommunicator::SetColor(QString color)
{

}


