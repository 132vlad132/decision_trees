#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include "QmlCommunicator.h"
#include <QQmlContext>
#include <QQmlComponent>

#include "dummyHeader.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QMLCommunicator communicator;

    engine.rootContext()->setContextProperty("communicator", &communicator);


    QQmlComponent component(&engine, "qmls\\main.qml");

    communicator.SetComponent(&component);

    //dummy definition in order to check that common headers are included
    dummyStructure* ptr;
    //end of dummy
    return app.exec();
}
