TEMPLATE = lib
TARGET = TreeImplementation

QT       -= gui
CONFIG += staticlib c++11

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += inc

HEADERS += \
    $$PWD/inc/NodeValue.h \
    $$PWD/inc/Node.h

SOURCES += \
    $$PWD/src/NodeValue.cpp \
    $$PWD/src/Node.cpp
