#ifndef NODE_H
#define NODE_H

#include <QString>
#include <QList>

#include "NodeValue.h"

class Node
{
public:
    Node();

private:
    QString id;
    QList<Node*> _children;
    NodeValue _value;
//    NodeType _type;
};

#endif // NODE_H
