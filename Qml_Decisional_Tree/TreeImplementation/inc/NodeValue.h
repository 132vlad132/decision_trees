#ifndef NODEVALUE_H
#define NODEVALUE_H

#include <QString>

class NodeValue
{
public:
    NodeValue();
    NodeValue(double weight, double value, QString data);

    double Value() const;
    void Value(const double val);

    double Weight() const;
    void Weight(const double val);

    QString Data() const;
    void Data(const QString data);

private:
    double _weight;
    double _value;
    QString _data;
};

#endif // NODEVALUE_H
