#include "NodeValue.h"

NodeValue::NodeValue():
    _weight(0),
    _value(0),
    _data()
{

}

NodeValue::NodeValue(double weight, double value, QString data):
    _weight(weight),
    _value(value),
    _data(data)
{

}

double NodeValue::Value() const
{
    return _value;
}

void NodeValue::Value(const double val)
{
    _value = val;
}

double NodeValue::Weight() const
{
    return _weight;
}

void NodeValue::Weight(const double val)
{
    _weight = val;
}

QString NodeValue::Data() const
{
    return _data;
}

void NodeValue::Data(const QString data)
{
    _data = data;
}
